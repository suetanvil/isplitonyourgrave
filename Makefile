

all: install

clean:
	-rm -rf public

install: clean
	mkdir public
	cp `ls -1 src/* | grep -v '~$$'` public/
